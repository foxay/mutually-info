
const getFilterControl = () => document.getElementById("app-content-list-filter-input");
const getFilterCleaner = () => document.getElementById("app-content-list-cleaner");

const setFilterValueAndFilter = (newValue) => {
    const filterInput = getFilterControl();
    if(!filterInput){
        return;
    }
    filterInput.value = newValue;
    filter();
    onFilterChange();
};

function filter(){
    const filterValue = getFilterControl().value;
    const items = document.getElementById("app-content-list-container").children;
    if(!filterValue){
        for(let i = 0; i < items.length; i++){
            items[i].className = "app-content-list-item"
        }
        return;
    }

    for(let i = 0; i < items.length; i++){
        const item = items[i];
        const keywords = JSON.parse(item.dataset.keywords);
        item.className = "app-content-list-item-hidden";
        for(let j = 0; j < keywords.length; j++){
            if(keywords[j].toLowerCase().includes(filterValue.toLowerCase())
                || filterValue.toLowerCase().includes(keywords[j].toLowerCase())){
                item.className = "app-content-list-item";
            }
        }
    }
}

function addKeywordToSearch(keyword){
    clearFilterInput();
    setFilterValueAndFilter(`${getFilterControl().value} ${keyword}`);
}

function clearFilterInput(){
    setFilterValueAndFilter("");
}

function onFilterChange(){
    const filterInput = getFilterControl();
    const cleaner = getFilterCleaner();
    cleaner.style.display = filterInput.value ? "block" : "none";
}