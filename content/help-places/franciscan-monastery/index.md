---
title: Jadłodajnia przy Klasztorze Franciszkanów
date: 2022-07-20
draft: false
region:
  - Wrocław
  - aleja Kasprowicza 26
  - Karłowice
  - 51-131
tags:
  - instytucja zakonna
  - program PEAD
  - wsparcie finansowe
  - konsultacje
categories:
  - jadłodajnia
description: Darmowe ciepłe posiłki, herbata, kawa
---

Al. Kasprowicza 26, Karłowice 51-131 Wrocław

Telefon: +48 713 273 593

Czynne: od poniedziałku do soboty, od 12:00 do wydania ostatniego posiłku

Darmowe ciepłe posiłki, herbata, kawa. Raz w miesiącu wydawana jest żywność w ramach programu PEAD, jest to mleko, mąka, makaron, ser żółty i topiony, cukier, kasza. Organizacja kolacji wigilijnej. Paczka żywnościowa ze środkami czystości. Wsparcie finansowe: zakup wyprawek do szkoły dla dzieci z rodzin wielodzietnych, zakup odzieży (zwłaszcza okres zimowy), wykup lekarstw, w szkole podstawowej nr 20 opłacany jest obiad dla kilku dzieci. Opieka duszpasterska i poradnie, rekolekcje w adwencie, indywidualne rozmowy, od stycznia 2007 działa punkt konsultacyjny z zakresu pomocy społecznej, w soboty wydawana jest odzież używana.

http://antoni.w-w.pl/index.php/kuchnia-charytatywna/