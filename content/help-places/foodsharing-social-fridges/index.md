---
title: Lodówki społeczne Wrocław
date: 2022-07-20
draft: false
region:
  - Wrocław
tags:
  - inicjatywa społeczna
categories:
  - lodówki
description: Świeże produkty spożywcze
---


Pilczyce:
- aleja Śląska 1, czynne całą dobę. Lodówka zlokalizowana jest przy Stadionie Miejskim, od ul. Królewieckiej.

Kozanów:
- ul. Pilczycka 47, czynne całą dobę. Lodówka znajduje się na tyłach Wrocławskiego Klubu Anima, od strony parkingu.
- ul.Kolista 14K, czynne całą dobę. Punkt znajduje się tuż obok paczkomatu.

Osobowice:
- ul. Osobowicka 129, czynne całą dobę. Lodówka przy kościele św. Teresy od Dzieciątka Jezus.

Muchobór:
- róg Mińska/Zagony 63, czynne całą dobę. Zlokalizowana jest na rogu ulic Mińskiej i Zagony, przy Zgromadzeniu Sióstr Służebniczek (ul. Zagony 63).

Grabiszyn:
- ul. Grabiszyńska 184 (Grabiszynek), czynne całą dobę. Lodówka znajduje się koło budynku administracji Centrum 
Historii Zajezdnia (od strony ul. Inżynierskiej).
- aleja Hallera 52 (Grabiszyn), czynne całą dobę. Znajduje się przy głównym wejściu do galerii.
- ul. Grabiszyńska 56 (Grabiszyn), czynna całą dobę.
- ul. Gajowicka 96a (Powstańców Śląskich), czynne całą dobę. Lodówka przed siedzibą Rady Osiedla Powstańców Śląskich, niedaleko kościoła przy ul. Kruczej. Jest widoczna z ulicy

Szczepin:
- ul. Strzegomska 49, czynne od poniedziałku do piątku w godz. 8:00-16:00. W soboty i niedziele nieczynne. Lodówka znajduje się tuż przy Wrocławskim Centrum Integracji. Należy przejść przez metalową bramkę i kierować się prosto do głównego wejścia. Lodówka znajduje się tuż za małym, szarym budynkiem po lewej stronie.

Nadodrze:
- ul. Ludwika Rydygiera 25a, czynna całą dobę. Lodówka w podwórzu ul. Rydgiera, tuż przy HART – Hostel&Art

Poświętne:
- ul. Kamieńskiego 190, czynna całą dobę. Punkt znajduje się przy Radzie Osiedla.

Ołbin:
- ul. Odona Bujwida 51, czynne w godz. 8-19. Lodówka znajduje się przy kościele.
- ul. Namysłowska 8, czynne w godzinach otwarcia CB Grafit. Lodówka znajduje się w przedsionku wejścia do Centrum Biznesowego Grafit.
- ul. Jedności Narodowej 187a, czynne całą dobę. Trzeba wejść na podwórko wjazdem dla samochodów, lodówka znajduje się po prawej stronie.
- ul. Prusa 37, czynne całą dobę. Lodówka znajduje się na rogu ulic Prusa i Wyszyńskiego, przy Rewizja grupa projektowa.

Przedmieście oławskie:
- ul. Hercena 13, czynna całą dobę. Znajduje się po prawej stronie, przy wejściu do bramy Przed Pokój H13.

Huby:
- ul. Hubska 8-16, czynna całą dobę. Lodówka mieści się przy Urzędzie Miasta.

Brochów:
- ul. Biegła 2, czynne całą dobę. Lodówka znajduje się przy parafii pw. św. Jerzego Męczennika i Podwyższenia Krzyża Świętego.

Psie Pole:
- ul. Krzywoustego 286, czynna całą dobę. 

Swojczyce:
- ul. Swojczycka 82, czynna całą dobę. Lodówka znajduje się tuż przy wejściu do sklepu Eko Tytka. 
- ul. Swojczycka 118, czynna całą dobę. Lodówka znajduje się z przodu budynku, między wejściem do Rewiru, a wejściem do Rady Osiedla. 