---
title: Fundacja Weźpomóż
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Poznańska 58
  - Stare Miasto
  - 53-630
tags:
  - fundacja
  - lodówki społeczne
categories:
  - jadłodajnia
description: Paczki z żywiością, lodówki społeczne
---

Poznańska 58, Stare Miasto 53-630 Wrocław

Telefon: +48 508 101 016

Czynne: poniedziałek, czwartek i piątek, od ok. godziny 13.00

Wydawanie żywności dla potrzebujących. Stawianie Społecznych Lodówek. Wydawka paczkek z żywnością 3 razy w tygodniu. 

https://wezpomoz.pl/ 

https://wezpomoz.pl/mapa-lodowek 