---
title: Kuchnia charytatywna fundacji Sancta Familia
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Monte Cassino 64
  - Śródmieście
  - 51-681
tags:
  - fundacja
categories:
  - jadłodajnia
description: Ciepłe posiłki, poradnia rodzinna,psychologiczna, pedagogiczna, w temacie uzależnień, poradnictwo prawne
---

Monte Cassino 64, Śródmieście 51-681 Wrocław 

Telefon: +48 517 453 575 

E-mail: fundacja@safa.org.pl

Czynne: poniedziałek – sobota, 12:00 – 13:30

Codziennie ponad 200 posiłków. Kuchnia charytatywna jest wspierana przez gminę Wrocław. Współpracujemy także z Bankiem Żywności, który przekazuje żywność w ramach Programu Operacyjnego Pomoc Żywnościowa 2014-2020. Współdziałanie z Miejskim Ośrodkiem Pomocy Społecznej. Obok kuchni charytatywnej Fundacja we współpracy z Gminą Wrocław prowadzi Poradnię Rodzinną zapewniającą wsparcie psychologa, pedagoga terapeuty uzależnień oraz świadczący poradnictwo prawne Ośrodek Poradnictwa Obywatelskiego.

https://sanctafamilia.pl/kuchnia-charytatywna/
