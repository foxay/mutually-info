---
title: Kuchnia Charytatywna przy parafii pw. św. Elżbiety
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Grabiszyńska 103
  - Fabryczna
  - 53-439
tags:
  - instytucja kościelna
categories:
  - jadłodajnia
description: Ciepłe posiłki
---

Grabiszyńska 103, Fabryczna 53-439 Wrocław

Telefon: +48 717 833 794

Czynne: UZUPEŁNIĆ

W budynku parafialnym, bardzo blisko kościoła jest przygotowywany posiłek. Posiłki są wydawane 11 miesięcy w ciągu roku, 6 dni w tygodniu. Dziennie wydawanych jest najczęściej od 180 do 200 posiłków. Posiłek stanowi gorąca zupa i pieczywo.

http://elzbieta.org/?p=4286 