---
title: Zupa na Wolności
date: 2022-07-20
draft: false
region:
  - Wrocław
  - plac Wolności
  - Stare Miasto
  - 50-071
tags:
  - inicjatywa społeczna
categories:
  - wydawka
description: Ciepłe posiłki, napoje, odzież, produkty domowe
---

Plac Wolności, Stare Miasto 50-071 Wrocław

Czynne: niedziela, od 17:00 

W każdą niedzielę między 13:30 a 16:30 gotowanie zupy. Wydawka wraz z ciastem, kawą, herbatą od godziny 17:00. Pomoc w zbiórkach ubrań, butów, środków czystości, materiałów opatrunkowych i produktów pierwszej potrzeby, a także w organizuji pomocy medycznej.

https://www.facebook.com/zupanawolnosci 