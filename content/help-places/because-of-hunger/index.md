---
title: Z Głodu
date: 2022-07-20
draft: false
region:
  - Wrocław
  - ulica Jagiellończyka 10
  - Nadodrze
  - 50-222
tags:
  - inicjatywa społeczna
categories:
  - wydawki na ulicy
description: Świeże produkty spożywcze
---

Jagiellończyka 10, Nadodrze, Wrocław

Czynne: czwartek, 11:30

Na terenie Centrum Reanimacji Kultury wydawka świeżych produktów spożywczych.